var searchData=
[
  ['tactbot_61',['TactBot',['../classTactBot_1_1TactBot.html',1,'TactBot']]],
  ['tactbot_20android_20application_62',['TactBot Android Application',['../page3.html',1,'']]],
  ['tactbot_2epy_63',['TactBot.py',['../TactBot_8py.html',1,'']]],
  ['togglebt_64',['toggleBT',['../classcom_1_1example_1_1tactbot_1_1Connectivity.html#a3ec74216b9948deab75313977946df09',1,'com::example::tactbot::Connectivity']]],
  ['togglediscovery_65',['toggleDiscovery',['../classcom_1_1example_1_1tactbot_1_1Connectivity.html#aef78d07212691d1a26d98e9846fd8e6d',1,'com::example::tactbot::Connectivity']]],
  ['turret_66',['Turret',['../classturret_1_1Turret.html',1,'turret']]],
  ['turret_20system_67',['Turret System',['../page2.html',1,'']]],
  ['turret_2epy_68',['turret.py',['../turret_8py.html',1,'']]],
  ['turret_5fangle_69',['turret_angle',['../classservo_1_1Servo.html#a44df1fb51df1cd28025f0fe3a1def241',1,'servo::Servo']]]
];
