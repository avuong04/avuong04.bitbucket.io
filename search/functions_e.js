var searchData=
[
  ['spin_124',['spin',['../classmotor_1_1Motor.html#a3bc5b51fe4f7440a84c770843e3efe15',1,'motor::Motor']]],
  ['start_125',['start',['../classcom_1_1example_1_1tactbot_1_1BluetoothUtil.html#a2caf6a589eff6c44ec2ca4967ead3f57',1,'com.example.tactbot.BluetoothUtil.start()'],['../classcamera_1_1Camera.html#ab27e1290df7b6836a19a8deb7659319b',1,'camera.Camera.start()']]],
  ['startbtconnection_126',['startBTConnection',['../classcom_1_1example_1_1tactbot_1_1Connectivity.html#a778f8dcc413e1f345252e98a4c90e28e',1,'com::example::tactbot::Connectivity']]],
  ['startclient_127',['startClient',['../classcom_1_1example_1_1tactbot_1_1BluetoothUtil.html#af26a5592b7d789b2795a8284721afc92',1,'com::example::tactbot::BluetoothUtil']]],
  ['startconnection_128',['startConnection',['../classcom_1_1example_1_1tactbot_1_1Connectivity.html#a0d8855ae8c4190f34e8d9d1b9d5333e1',1,'com::example::tactbot::Connectivity']]],
  ['steer_5fleft_129',['steer_left',['../classRover_1_1Rover.html#aaf237e98a55b7df18af9a63bae932c7a',1,'Rover::Rover']]],
  ['steer_5fright_130',['steer_right',['../classRover_1_1Rover.html#aa4dc1e7fd38aa5740ea16d437cfb30b8',1,'Rover::Rover']]],
  ['stop_131',['stop',['../classRover_1_1Rover.html#a1f66bdaf495256bccdef6903f58f8b63',1,'Rover::Rover']]],
  ['surfacechanged_132',['surfaceChanged',['../classcom_1_1example_1_1tactbot_1_1JoyStick.html#ad3b933a32025dea80d27449ded279005',1,'com::example::tactbot::JoyStick']]],
  ['surfacecreated_133',['surfaceCreated',['../classcom_1_1example_1_1tactbot_1_1JoyStick.html#a39c44b56e841b276d9df393470992456',1,'com::example::tactbot::JoyStick']]],
  ['surfacedestroyed_134',['surfaceDestroyed',['../classcom_1_1example_1_1tactbot_1_1JoyStick.html#acc42559f56244e0f95e538e683371ebe',1,'com::example::tactbot::JoyStick']]]
];
