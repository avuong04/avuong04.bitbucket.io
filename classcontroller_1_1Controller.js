var classcontroller_1_1Controller =
[
    [ "__init__", "classcontroller_1_1Controller.html#a1ddf903aab9b45c43db36da92ea22df4", null ],
    [ "adjustSig", "classcontroller_1_1Controller.html#a6d9faf513edd3b8d91a480571a0abb14", null ],
    [ "run", "classcontroller_1_1Controller.html#ae659ef4b1428f43ef5d1d014f9c7633d", null ],
    [ "setKDGain", "classcontroller_1_1Controller.html#a1c9244c3a8c179eb142edd0f3e5702f6", null ],
    [ "setKDGain", "classcontroller_1_1Controller.html#a1c9244c3a8c179eb142edd0f3e5702f6", null ],
    [ "setKPGain", "classcontroller_1_1Controller.html#a158a2dbbcb932e707f8caaad701478d7", null ],
    [ "setSetPoint", "classcontroller_1_1Controller.html#afe9a2343f81665d7b28d19568dc97513", null ],
    [ "encoder", "classcontroller_1_1Controller.html#afdd295942bf7d13e7d50eb98f50de85b", null ],
    [ "error", "classcontroller_1_1Controller.html#ae256ace611fdeaefb7e8d2f752c6885b", null ],
    [ "kd", "classcontroller_1_1Controller.html#ac875c7949e47f52af53ce22632888444", null ],
    [ "ki", "classcontroller_1_1Controller.html#a771b70592fdd0103c080925b617a96fa", null ],
    [ "kp", "classcontroller_1_1Controller.html#aa58cbf244217b5c1da94b06f71da0512", null ],
    [ "motor", "classcontroller_1_1Controller.html#aeb3080380e6c1d87d79f19b1831cd620", null ],
    [ "previousError", "classcontroller_1_1Controller.html#ae0312a657deb95616b22a95298bed346", null ],
    [ "set", "classcontroller_1_1Controller.html#a3a77510a6a52481edd7e65b740f77077", null ],
    [ "setPoint", "classcontroller_1_1Controller.html#a0b01e989bdc352cc1585212b0515e13d", null ],
    [ "sumError", "classcontroller_1_1Controller.html#a459e29c585ba718470378e3af2a08258", null ]
];