var classmotor_1_1Motor =
[
    [ "__init__", "classmotor_1_1Motor.html#ace47ac736d7f960a92ea1369a1783cd6", null ],
    [ "ccw", "classmotor_1_1Motor.html#a1fac4b55a9603ccc2cadcc9f6fd9c667", null ],
    [ "cw", "classmotor_1_1Motor.html#a478ecefb5897f525499942663e2cdd04", null ],
    [ "disable", "classmotor_1_1Motor.html#a300c7f5aa927caaba4f5e4008b5aa509", null ],
    [ "enable", "classmotor_1_1Motor.html#af50258273263df81f6aed625ca7d4c6d", null ],
    [ "hold", "classmotor_1_1Motor.html#aa9a31865702102109c87236a299c8598", null ],
    [ "spin", "classmotor_1_1Motor.html#a3bc5b51fe4f7440a84c770843e3efe15", null ],
    [ "enable_pin", "classmotor_1_1Motor.html#a1e6aec66fa089c1e6258ec212326e99f", null ],
    [ "in1_pin", "classmotor_1_1Motor.html#a14ab7dfe009427822865bc5cbbe0b341", null ],
    [ "in2_pin", "classmotor_1_1Motor.html#a891a5dc43b7b584c165202f4c34308e1", null ],
    [ "inMotion", "classmotor_1_1Motor.html#a8e6906e8170e2d18326fda80010213aa", null ],
    [ "speed", "classmotor_1_1Motor.html#a8296fecbf40d7711ca168037892a5e09", null ]
];