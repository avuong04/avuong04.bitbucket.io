var classcom_1_1example_1_1tactbot_1_1Connectivity =
[
    [ "listDevices", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a8edd49c438c9c69390ef061cf06e61dd", null ],
    [ "onCreate", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a3336f9219ae2b055559c1ecf470c2214", null ],
    [ "onItemClick", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a6a67bac0a84b6b783e5dfa53345b4a6a", null ],
    [ "startBTConnection", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a778f8dcc413e1f345252e98a4c90e28e", null ],
    [ "startConnection", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a0d8855ae8c4190f34e8d9d1b9d5333e1", null ],
    [ "toggleBT", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a3ec74216b9948deab75313977946df09", null ],
    [ "toggleDiscovery", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#aef78d07212691d1a26d98e9846fd8e6d", null ],
    [ "bluetoothAdapter", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a8be3637b20c8b0041480bb4e60f19c44", null ],
    [ "bluetoothDevice", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a210105c9e0c949dcb728d778c333c121", null ],
    [ "bluetoothUtil", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a655110d8f3c51a900bfbcc45c451cad9", null ],
    [ "btDevices", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a646beb48140eb6a8b892d7ee8bf1b057", null ],
    [ "deviceListAdapter", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a4999f5dbb525eaddd09e01af3f92f4cf", null ],
    [ "listView", "classcom_1_1example_1_1tactbot_1_1Connectivity.html#a8af0502372938f4ce00c8690a28399a7", null ]
];