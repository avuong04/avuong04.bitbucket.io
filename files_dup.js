var files_dup =
[
    [ "camera.py", "camera_8py.html", [
      [ "Camera", "classcamera_1_1Camera.html", "classcamera_1_1Camera" ]
    ] ],
    [ "motor.py", "motor_8py.html", [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "Rover.py", "Rover_8py.html", "Rover_8py" ],
    [ "servo.py", "servo_8py.html", [
      [ "Servo", "classservo_1_1Servo.html", "classservo_1_1Servo" ]
    ] ],
    [ "TactBot.py", "TactBot_8py.html", "TactBot_8py" ],
    [ "turret.py", "turret_8py.html", [
      [ "Turret", "classturret_1_1Turret.html", "classturret_1_1Turret" ]
    ] ]
];