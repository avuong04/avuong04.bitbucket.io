var hierarchy =
[
    [ "com.example.tactbot.BluetoothConnection", "classcom_1_1example_1_1tactbot_1_1BluetoothConnection.html", null ],
    [ "com.example.tactbot.BluetoothUtil", "classcom_1_1example_1_1tactbot_1_1BluetoothUtil.html", null ],
    [ "SurfaceHolder.Callback", null, [
      [ "com.example.tactbot.JoyStick", "classcom_1_1example_1_1tactbot_1_1JoyStick.html", null ]
    ] ],
    [ "camera.Camera", "classcamera_1_1Camera.html", null ],
    [ "IMU.IMU", "classIMU_1_1IMU.html", null ],
    [ "com.example.tactbot.JoyStick.JoystickListener", "interfacecom_1_1example_1_1tactbot_1_1JoyStick_1_1JoystickListener.html", [
      [ "com.example.tactbot.PilotMode", "classcom_1_1example_1_1tactbot_1_1PilotMode.html", null ]
    ] ],
    [ "motor.Motor", "classmotor_1_1Motor.html", null ],
    [ "AdapterView.OnItemClickListener", null, [
      [ "com.example.tactbot.Connectivity", "classcom_1_1example_1_1tactbot_1_1Connectivity.html", null ]
    ] ],
    [ "View.OnTouchListener", null, [
      [ "com.example.tactbot.JoyStick", "classcom_1_1example_1_1tactbot_1_1JoyStick.html", null ]
    ] ],
    [ "Rover.Rover", "classRover_1_1Rover.html", null ],
    [ "servo.Servo", "classservo_1_1Servo.html", null ],
    [ "TactBot.TactBot", "classTactBot_1_1TactBot.html", null ],
    [ "turret.Turret", "classturret_1_1Turret.html", null ],
    [ "AppCompatActivity", null, [
      [ "com.example.tactbot.Connectivity", "classcom_1_1example_1_1tactbot_1_1Connectivity.html", null ],
      [ "com.example.tactbot.MainActivity", "classcom_1_1example_1_1tactbot_1_1MainActivity.html", null ],
      [ "com.example.tactbot.PilotMode", "classcom_1_1example_1_1tactbot_1_1PilotMode.html", null ]
    ] ],
    [ "ArrayAdapter", null, [
      [ "com.example.tactbot.DevListAdapter", "classcom_1_1example_1_1tactbot_1_1DevListAdapter.html", null ],
      [ "com.example.tactbot.DeviceListAdapter", "classcom_1_1example_1_1tactbot_1_1DeviceListAdapter.html", null ]
    ] ],
    [ "SurfaceView", null, [
      [ "com.example.tactbot.JoyStick", "classcom_1_1example_1_1tactbot_1_1JoyStick.html", null ]
    ] ]
];