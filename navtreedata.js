/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "TactBot", "index.html", [
    [ "Introduction", "index.html#sec_port", null ],
    [ "Goals and Objectives", "index.html#sec_gols_objs", null ],
    [ "Source Code", "index.html#sec_github", null ],
    [ "TactBot visual", "index.html#sec_app", null ],
    [ "Rover System", "page1.html", [
      [ "Desription", "page1.html#sec_descr4", null ],
      [ "Related Files", "page1.html#sec_files9", null ],
      [ "Operations", "page1.html#sec_op4", null ],
      [ "Rover visual", "page1.html#sec_app2", null ],
      [ "Source Code Link", "page1.html#sec_link4", null ]
    ] ],
    [ "TactBot Android Application", "page12.html", [
      [ "Desription", "page12.html#sec_descr12", null ],
      [ "Related Files", "page12.html#sec_files12", null ],
      [ "Operations", "page12.html#sec_op12", null ],
      [ "App visual", "page12.html#sec_app12", null ],
      [ "Source Code Link", "page12.html#sec_link12", null ]
    ] ],
    [ "Turret System", "page2.html", [
      [ "Desription", "page2.html#sec_descr5", null ],
      [ "Related Files", "page2.html#sec_files5", null ],
      [ "Operations", "page2.html#sec_op5", null ],
      [ "Turret visual", "page2.html#sec_app1", null ],
      [ "Source Code Link", "page2.html#sec_link5", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Rover_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';